from django.urls import include, path
from rest_framework.routers import DefaultRouter

from .views import ConvertRateViewset, RateCardViewset

router = DefaultRouter()
router.register(
    r"ratecard",
    RateCardViewset,
    basename="ratecard",
)
router.register(
    r"rate",
    ConvertRateViewset,
    basename="rate",
)

urlpatterns = [path("", include(router.urls))]
