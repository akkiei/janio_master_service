from datetime import datetime

from django.forms import ValidationError
from djmoney import settings
from rest_framework import serializers


def validate_currency(currency):
    currencies = [row[0] for row in settings.CURRENCY_CHOICES]
    if currency not in currencies:
        raise ValidationError(f"{currency} is not a valid currency)")


def validate_date(date):
    try:
        datetime.strptime(date, "%Y-%m-%d")
    except Exception:
        raise ValidationError(f"{date} is not a valid format ('%Y-%m-%d'))")


class RateSerializer(serializers.Serializer):
    currency = serializers.CharField(validators=[validate_currency])
    value = serializers.DecimalField(max_digits=10, decimal_places=2)


class RateCardSerializer(serializers.Serializer):
    rate_card_name = serializers.CharField()
    rates = serializers.ListField(child=RateSerializer())


class ConvertRateSerializer(serializers.Serializer):
    base_currency = serializers.CharField(validators=[validate_currency])
    base_currency_value = serializers.DecimalField(max_digits=10, decimal_places=2)
    target_currency = serializers.CharField(validators=[validate_currency])
    backend_name = serializers.CharField(default=None)
    rate_card_name = serializers.CharField(default=None)
    date = serializers.CharField(default=None, validators=[validate_date])
