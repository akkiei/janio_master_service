from datetime import datetime
from decimal import Decimal

import requests
from django.conf import settings
from django.utils import timezone

from exchange.backends.base import BaseExchangeBackend


class MasBackend(BaseExchangeBackend):
    name = "mas"

    def __init__(self, url=settings.MAS_URL):
        self.url = url
        today = datetime.now(tz=timezone.utc).strftime("%Y_%m_%d")
        self.rate_card_name = "_".join(["weekly", today])

    def get_params(self):
        return {}

    def get_rates(self, **params):
        url = self.get_url(**params)
        response = requests.get(url)

        response_data = response.json()
        rates = {}
        for key, value in response_data.get("result").get("records")[0].items():
            if "sgd" in key:
                key_split = key.split("_")

                if len(key_split) == 2:
                    conversion_rate = Decimal(value)
                elif len(key_split) == 3:
                    conversion_rate = Decimal(value) / 100
                rates[key_split[0].upper()] = 1 / conversion_rate
        return rates
