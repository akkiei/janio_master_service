from exchange.backends.base import BaseExchangeBackend


class JanioBackend(BaseExchangeBackend):
    name = "janio"

    def __init__(self, rate_card_name, rates):
        self.rate_card_name = rate_card_name
        self.rates = rates

    def get_params(self):
        return {}

    def get_rates(self, **params):
        return self.rates
