# Generated by Django 3.2.13 on 2022-06-30 15:25

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('partner', '0011_auto_20220630_1502'),
    ]

    operations = [
        migrations.AlterField(
            model_name='partner',
            name='password',
            field=models.CharField(max_length=200),
        ),
        migrations.AlterField(
            model_name='partner',
            name='public_key',
            field=models.CharField(default='1d8944b5361d47ba926736c70e58191d', max_length=100, unique=True),
        ),
    ]
