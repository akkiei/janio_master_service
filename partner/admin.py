from django.contrib.auth.admin import UserAdmin
from django.contrib import admin
from django import forms
from django.contrib.auth.hashers import make_password

from .models import Partner


class PartnerAdmin(admin.ModelAdmin):

	search_fields = ('username',)

	class PartnerForm(forms.ModelForm):
		password = forms.CharField(widget=forms.PasswordInput(render_value=True))

		class Meta:
			model = Partner
			fields = ('username', 'password', 'public_key', 'is_active', 'is_superuser', 'is_staff', 'disable_zones',
			          'min_threshold', 'max_threshold',)

		def save(self, commit=True):

			user = super().save(commit=False)
			user.set_password(self.cleaned_data["password"])
			if commit:
				user.save()
			return user

	def get_form(self, request, obj=None, change=False, **kwargs):
		return self.PartnerForm


admin.site.register(Partner, PartnerAdmin)
