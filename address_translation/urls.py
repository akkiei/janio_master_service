from django.urls import path, include
from rest_framework.routers import DefaultRouter

from .views import TranslationView

router = DefaultRouter()
router.register(r'get_translation', TranslationView, basename='translation')

urlpatterns = [
	path('', include(router.urls))
]
