from rest_framework import viewsets, status
from rest_framework.response import Response
from django.db.models import F

from .models import AddressTranslation, AddressTranslationLogs
from .serializers import AddressTranslationRequestSerializer
from .translation import AddressTranslator as Translator


class TranslationView(viewsets.ViewSet):
	# get address translation for any given raw address
	# Todo:  can use different viewset since this one has pre-defined method name for each type of HTTP request
	def create(self, request):
		data = request.data
		serializer = AddressTranslationRequestSerializer(data=data)
		serializer.is_valid(raise_exception=True)
		validated_data = serializer.validated_data
		translator = Translator(city=validated_data.get('city'), state=validated_data.get('state'),
		                        country=validated_data.get('country'), postal_code=validated_data.get('postal_code'))
		try:
			translated_address = translator.translate()
			# address_translation_instance = AddressTranslation(translated_address)
			# Todo: override the save method for AddressTranslation
			# at_instance = address_translation_instance.save()
			# TODO : make logging async using celery tasks
			at_logs_instance = AddressTranslationLogs(address_translation=translated_address,
			                                          raw_address="{},{},{},{}".format(validated_data.get('city', ''),
			                                                                           validated_data.get('state', ''),
			                                                                           validated_data.get('country', ''),
			                                                                           validated_data.get('postal_code', '')
			                                                                           ))
			at_logs_instance.save()
		except ValueError as ve:
			return Response({"error": str(ve)}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
		# TODO : change it to 200 after removing the db call to store the address translation
		response_obj = {
			"country_id": translated_address.country_id.country_id,
			"country_name": translated_address.country_id.country_name,
			"country_confidence_score": translated_address.country_score,
			"state_id": translated_address.state_id.state_id,
			"state_name": translated_address.state_id.state_name,
			"state_confidence_score": translated_address.state_score,
			"city_id": translated_address.city_id.city_id,
			"city_name": translated_address.city_id.city_name,
			"city_confidence_score": translated_address.city_score,
			"postal_prefix": translated_address.postal_prefix
		}
		return Response({"translated_address": response_obj}, status=status.HTTP_201_CREATED)

# translation logic will go here or
