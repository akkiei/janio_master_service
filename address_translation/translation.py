from difflib import SequenceMatcher

from django.db.models import Q as db_Q
from elasticsearch import Elasticsearch, NotFoundError
from elasticsearch.helpers import bulk
from elasticsearch_dsl import MultiSearch, Q, Search

from janio_master_service import settings
from location.models import City, Country, State

from .data import (
    COUNTRY_CITY_LEVEL_POSTAL_PREFIX_DIGITS,
    COUNTRY_STATE_LEVEL_POSTAL_PREFIX_DIGITS,
)
from .models import AddressTranslation, AddressTranslationLogs


class AddressTranslator:
    COUNTRY_STATE_CITY_INDEX = "country_state_city_postal"

    def __init__(self, city, state, country, postal_code):
        self.city = self.original_city = city
        self.state = self.original_state = state
        self.country = self.original_country = country
        self.postal_code = self.original_postal_code = postal_code
        self.postal_prefix_list = None
        self.city_score = 0
        self.state_score = 0
        self.country_score = 0
        self.avg_score = 0

    def translate(self):
        self._clean_address()  # clean extra whitespace or numbers or special characters in address
        self._correct_address()  # find the closest matching country/state/city for give address
        return (
            self._get_final_translation()
        )  # get the final country/state/city ids of each along with their scores

    def _clean_address(self):
        # todo: use difflib's sequncematcher for
        self._clean_country()
        self._clean_state()
        self._clean_city()
        self._clean_postal_code()

    def _correct_address(self):
        es_result = (
            self._elastic_search_address()
        )  # todo: search the closest matching result by checking the score and return the result with maximum score
        # get result with the highest score
        highest_rated_response = self._rate_responses(es_result)
        self._rate_translation(highest_rated_response[0].get("data", None))
        final_translation = self._get_final_translation()
        return final_translation

    def _elastic_search_address(self, postal_prefix_level="city"):
        # search using elastic search
        # rate results and get the one with maximum score
        if postal_prefix_level == "city":  # TODO: find a better way to do this
            postal_prefix = self.get_country_city_level_postal_code_prefix()
        else:
            postal_prefix = self.get_country_state_level_postal_code_prefix()

        elastic_search = Elasticsearch(settings.ELASTICSEARCH_HOST)
        multi_search = MultiSearch(
            using=elastic_search, index=self.COUNTRY_STATE_CITY_INDEX
        )

        for state, city in [(self.state, self.city), (self.city, self.state)]:
            qs = Q("match", state_list=state)
            if city:
                qs |= Q("match", city_list=city)
            if postal_prefix:
                qs |= Q(
                    "match",
                    postal_prefix_list={
                        "query": postal_prefix,
                        "minimum_should_match": "90%",
                    },
                )

            must_query = [Q("match", country=self.country), Q(qs)]
            s = Search().query(Q("bool", must=must_query))
            multi_search = multi_search.add(s)

        search_result = multi_search.execute()
        return search_result

    def get_country_city_level_postal_code_prefix(self):
        if not self.postal_code:
            return ""
        digits = COUNTRY_CITY_LEVEL_POSTAL_PREFIX_DIGITS.get(self.country)
        if not digits:
            return ""
        return str(self.postal_code)[:digits]

    def get_country_state_level_postal_code_prefix(self):
        if not self.postal_code:
            return ""
        digits = COUNTRY_STATE_LEVEL_POSTAL_PREFIX_DIGITS.get(self.country)
        if not digits:
            return ""
        return str(self.postal_code)[:digits]

    def _get_final_translation(self):
        # TODO: commit to db -> address_translation
        country, state, city = self.get_country_state_city_list()

        at_instance = AddressTranslation(
            city_id=city,
            state_id=state,
            country_id=country,
            postal_prefix=self.postal_prefix_list,
            city_score=self.city_score,
            state_score=self.state_score,
            country_score=self.country_score,
            overall_score=self.avg_score,
        )
        at_instance.save()
        return at_instance
        # # TODO return the db instances of city,state and country along with their confidence score
        # return {
        # 	"country_id": country.country_id,
        # 	"country_name": country.country_name,
        # 	"country_confidence_score": self.country_score,
        # 	"state_id": state.state_id,
        # 	"state_name": state.state_name,
        # 	"state_confidence_score": self.state_score,
        # 	"city_id": city.city_id,
        # 	"city_name": city.city_name,
        # 	"city_confidence_score": self.city_score
        # }

    def _clean_country(self):
        self.country = self._remove_whitespaces(self.country)

    def _clean_state(self):
        self.state = self._remove_whitespaces(self.state)

    def _clean_city(self):
        if "city" in self.city:
            self.city = self.city.replace("city", " city ")
        self.city = self._remove_whitespaces(self.city)

    def _clean_postal_code(self):
        self.postal_code = self._remove_whitespaces(self.postal_code)

    def _rate_city_score(self):
        # TODO: use difftool to check how much it matches with the result
        return self.similar(self.city, self.original_city)

    def _rate_state_score(self):
        # TODO: use difftool to check how much it matches with the result
        return self.similar(self.state, self.original_state)

    def _rate_country_score(self):
        # TODO: use difftool to check how much it matches with the result
        return self.similar(self.country, self.original_country)

    def _rate_responses(self, es_responses):
        results = []
        for response in es_responses:
            result = response.to_dict()
            if result["hits"] and result["hits"]["max_score"]:
                score = result["hits"]["max_score"]
                hits_result = result["hits"]["hits"][0]["_source"]
                results.append({"score": score, "data": hits_result})
        results = sorted(results, key=lambda i: i["score"], reverse=True)
        return results

    def _rate_translation(self, highest_rated_translation):
        self.country = highest_rated_translation.get("country", None)
        self.state = highest_rated_translation.get("state", None)
        self.city = highest_rated_translation.get("city", None)
        self.postal_prefix_list = highest_rated_translation.get(
            "postal_prefix_list", None
        )

        self.city_score = self._rate_city_score()
        self.state_score = self._rate_state_score()
        self.country_score = self._rate_country_score()
        self.avg_score = (self.city_score + self.state_score + self.country_score) / 3

    def _remove_whitespaces(self, text):
        text = text or ""
        text = text.lower().strip()  # remove extra whitespace
        text = " ".join(
            [token for token in text.split() if token]
        )  # one space b/w the words
        return text

    def similar(self, a, b):
        return SequenceMatcher(None, a, b).ratio()

    def get_country_state_city_list(self):
        if self.country is None:
            # TODO add log() here
            raise ValueError("country can't be empty")

        countries_query = db_Q()
        states_query = db_Q()
        cities_query = db_Q()

        countries_query |= db_Q(country_name=self.country)
        if self.state:
            states_query |= db_Q(
                state_name=self.state, country__country_name=self.country
            )
        if self.city:
            cities_query |= db_Q(
                city_name=self.city,
                state__state_name=self.state,
                state__country__country_name=self.country,
            )

        country_name_country = {
            country.country_name: country
            for country in Country.objects.filter(countries_query)
        }
        country_state_name_state = {
            (state.country.country_name, state.state_name): state
            for state in State.objects.filter(states_query).select_related("country")
        }
        country_state_city_name_city = {
            (
                city.state.country.country_name,
                city.state.state_name,
                city.city_name,
            ): city
            for city in City.objects.filter(cities_query).select_related(
                "state", "state__country"
            )
        }

        country = country_name_country.get(self.country)
        state = (
            country_state_name_state.get((self.country, self.state))
            if self.state
            else None
        )
        city = (
            country_state_city_name_city.get((self.country, self.state, self.city))
            if self.city
            else None
        )

        return country, state, city
