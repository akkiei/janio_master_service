from rest_framework import viewsets, status
from rest_framework.response import Response
from django.db.models import F

from .models import City, Country, State


class CountryViewSet(viewsets.ViewSet):
	# add throttling here
	def list(self, request):
		query_set = Country.objects.filter().all().values('country_id', 'country_name')
		result = [{"Countries": list(query_set)}]
		return Response({"zones": result}, status=status.HTTP_200_OK)


class StateViewSet(viewsets.ViewSet):
	# add throttling here

	def list(self, request):
		query_set = State.objects.filter().values('state_id', 'state_name', country_name=F('country__country_name')).all()
		result = [{"States": list(query_set)}]
		return Response({"zones": result}, status=status.HTTP_200_OK)


class CityViewSet(viewsets.ViewSet):
	# add throttling here
	def list(self, request):
		query_set = City.objects.filter().values('city_id', 'city_name', state_name=F('state__state_name')).all()
		result = [{"Cities": list(query_set)}]
		return Response({"zones": result}, status=status.HTTP_200_OK)
