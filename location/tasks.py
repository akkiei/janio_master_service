from django.db import transaction

from location.models import Country, State, City, Location

# contains scripts to do few setups
from partner.models import Partner
from zone.models import Zone, ZoneLocation


def populate_country_state_city_with_json(model_name):
	import json

	for_bulk_create = []

	if model_name == 'Country':
		file = open('location/location_db_files/country.json')
		data = json.load(file)
		for country in data:
			id = country.get('country_id')
			name = country.get('country_name')
			for_bulk_create.append(Country(country_name=name, country_id=id))
		with transaction.atomic():
			Country.objects.bulk_create(for_bulk_create)

	if model_name == 'State':
		file = open('location/location_db_files/state.json')
		data = json.load(file)
		for country in data:
			state_id = country.get('state_id')
			state_name = country.get('state_name')
			country_id = country.get('country_id')
			country_instance = Country.objects.filter(country_id=country_id).first()
			for_bulk_create.append(State(state_id=state_id, state_name=state_name, country=country_instance))
		with transaction.atomic():
			State.objects.bulk_create(for_bulk_create)

	if model_name == 'City':
		file = open('location/location_db_files/city.json')
		data = json.load(file)
		for country in data:
			city_id = country.get('city_id')
			city_name = country.get('city_name')
			postal_prefix = country.get('postal_code_prefix')
			postal_prefix_len = country.get('postal_prefix_length')

			state_id = country.get('state_id')
			state_instance = State.objects.filter(state_id=state_id).first()
			for_bulk_create.append(
					City(city_id=city_id, city_name=city_name, state=state_instance, postal_prefix=postal_prefix,
					     postal_prefix_len=postal_prefix_len))
		with transaction.atomic():
			City.objects.bulk_create(for_bulk_create)

	if model_name == 'Location':
		file = open('location/location_db_files/location.json')
		data = json.load(file)
		for country in data:
			location_id = country.get('id')
			country_id = country.get('country_id')
			country_instance = Country.objects.filter(country_id=country_id).first()
			state_id = country.get('state_id')
			state_instance = State.objects.filter(state_id=state_id).first()
			city_id = country.get('city_id')
			city_instance = City.objects.filter(city_id=city_id).first()

			for_bulk_create.append(Location(country=country_instance, state=state_instance, city=city_instance,
			                                location_id=location_id))
		with transaction.atomic():
			Location.objects.bulk_create(for_bulk_create)

	if model_name == 'Zone':
		file = open('location/location_db_files/zones.json')
		data = json.load(file)
		for zone in data:
			zone_id = zone.get('id')
			name = zone.get('name')
			partner_id = zone.get('partner') or 2  # partner_id should come from auth
			partner = Partner.objects.filter(id=partner_id).first()
			start_date = zone.get('start_date')
			end_date = zone.get('end_date')

			for_bulk_create.append(
				Zone(zone_id=zone_id, name=name, start_date=start_date, end_date=end_date, partner=partner))
		with transaction.atomic():
			Zone.objects.bulk_create(for_bulk_create)

	if model_name == 'ZoneLocation':
		file = open('location/location_db_files/zone_location_bridge.json')
		data = json.load(file)
		for d in data:
			zone_id = d.get('zone_id')
			zone = Zone.objects.filter(zone_id=zone_id).first()
			location_id = d.get('zonelocation_id')
			location = Location.objects.filter(location_id=location_id).first()
			if zone and location:
				for_bulk_create.append(
					ZoneLocation(zone=zone, location=location)
				)
		with transaction.atomic():
			ZoneLocation.objects.bulk_create(for_bulk_create)
