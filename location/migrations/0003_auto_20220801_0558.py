# Generated by Django 3.2.13 on 2022-08-01 05:58

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('location', '0002_auto_20220630_0602'),
    ]

    operations = [
        migrations.AlterField(
            model_name='city',
            name='city_id',
            field=models.IntegerField(unique=True),
        ),
        migrations.AlterField(
            model_name='country',
            name='country_id',
            field=models.IntegerField(unique=True),
        ),
        migrations.AlterField(
            model_name='state',
            name='state_id',
            field=models.IntegerField(unique=True),
        ),
    ]
