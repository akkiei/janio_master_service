# Generated by Django 3.2.13 on 2022-06-28 13:39

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='City',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('city_id', models.IntegerField()),
                ('city_name', models.CharField(max_length=100)),
                ('postal_prefix', models.CharField(max_length=100, null=True)),
                ('postal_prefix_len', models.IntegerField(null=True)),
                ('created_at', models.DateField(auto_now_add=True)),
            ],
        ),
        migrations.CreateModel(
            name='Country',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('country_id', models.IntegerField()),
                ('country_name', models.CharField(max_length=100, unique=True)),
                ('created_at', models.DateField(auto_now_add=True)),
            ],
        ),
        migrations.CreateModel(
            name='State',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('state_id', models.IntegerField()),
                ('state_name', models.CharField(max_length=100)),
                ('created_at', models.DateField(auto_now_add=True)),
                ('country', models.ForeignKey(db_column='country_id', on_delete=django.db.models.deletion.CASCADE, to='location.country')),
            ],
        ),
        migrations.CreateModel(
            name='Location',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('location_id', models.IntegerField()),
                ('created_at', models.DateField(auto_now_add=True)),
                ('updated_at', models.DateField(auto_now_add=True)),
                ('city', models.ForeignKey(db_column='city_id', null=True, on_delete=django.db.models.deletion.CASCADE, to='location.city')),
                ('country', models.ForeignKey(db_column='country_id', null=True, on_delete=django.db.models.deletion.CASCADE, to='location.country')),
                ('state', models.ForeignKey(db_column='state_id', null=True, on_delete=django.db.models.deletion.CASCADE, to='location.state')),
            ],
        ),
        migrations.AddField(
            model_name='city',
            name='state',
            field=models.ForeignKey(db_column='state_id', on_delete=django.db.models.deletion.CASCADE, to='location.state'),
        ),
    ]
