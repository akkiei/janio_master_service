from django.contrib import admin
from .models import City, Country, State


# Register your models here.

class CityAdmin(admin.ModelAdmin):
	fields = ('city_name', 'state', 'postal_prefix', 'postal_prefix_len')


class StateAdmin(admin.ModelAdmin):
	fields = ('state_name', 'country')


class CountryAdmin(admin.ModelAdmin):
	fields = ['country_name']


admin.site.register(City, CityAdmin)
admin.site.register(State, StateAdmin)
admin.site.register(Country, CountryAdmin)
