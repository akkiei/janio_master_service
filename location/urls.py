from django.contrib import admin
from django.urls import path, include
from rest_framework.routers import DefaultRouter

from .views import CountryViewSet, StateViewSet, CityViewSet

router = DefaultRouter()
router.register(r'country', CountryViewSet, basename='country')
router.register(r'state', StateViewSet, basename='state')
router.register(r'city', CityViewSet, basename='city')

urlpatterns = [
	path('', include(router.urls))
]
